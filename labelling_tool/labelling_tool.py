import os
from threading import Timer
import webbrowser

import dash
from dash import dcc
from dash import html
from dash import Input, Output, State, ctx
import dash_bootstrap_components as dbc
from flask import Flask
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from waitress import serve

import config.config as cfg

config = cfg.get_config()
in_dir = cfg.get_in_dir(config)
out_dir = cfg.get_out_dir(config)
filename = cfg.get_csvfile(config)
filepath = os.path.join(in_dir, filename)
measured = cfg.get_measuredcolumns(config)
maximum_cs_models = cfg.get_cscolumns(config)

def read_new_file(filepath):
    print('TRIGGERED: read new file')
    global df
    df = pd.read_csv(filepath)
    #df.drop(columns = 'datetime.1', inplace = True)
    global days
    days = df['date'].unique()
    global df_plot
    df_plot = df[df['date']==days[0]]
    df_plot.reset_index(drop=True, inplace=True)
    global date
    date = df_plot['date'].iloc[0]
    global location
    location = df_plot['LocationID'].iloc[0]
    global filename
    filename = os.path.split(filepath)[1]


def plotly_plot(df, measured, cs, date, station):

    fig = go.Figure()

    for m in measured:
        fig.add_trace(go.Scatter(
            x=df['datetime'],
            y=df[m],
            name=m, 
            hovertemplate="%{y}%{_xother}",
            mode = 'lines+markers',
            marker_size = 3
        ))
    
    cs_colors = ['orange', 'magenta', 'cyan', 'red', 'yellow']
    for idx, model in enumerate(cs):
        fig.add_trace(go.Scatter(
            x=df['datetime'],
            y=df[model],
            name=model,
            marker = dict(color=cs_colors[idx], size = 3),
            hovertemplate="%{y}%{_xother}",
            mode = 'lines+markers'
        ))

    fig.update_layout(
        title='Comparison {} for station {}'.format(date, station),
        xaxis_title="Time (UTC)",
        yaxis_title="Irradiance",
        legend_title="Measurements",
        font=dict(
            family="Courier New, monospace",
            size=18
        ), 
        clickmode='event+select'
    )

    return fig

graph_alerts = html.Div(
    [
        dbc.Alert(
            [
                "You have reached the end of the dataset. Please change dataset."
            ],
            color="warning",
            id = "alert-end-of-dataframe",
            fade = False,
            is_open = False,
            dismissable = True
        ),
        dbc.Alert(
            [
                "You have reached the start of the dataset. To get to an earlier day, please change dataset"
            ],
            color="warning",
            id = "alert-start-of-dataframe",
            fade = False,
            is_open = False,
            dismissable = True
        )
    ]
)

io_alerts = html.Div(
    [
        dbc.Alert(
            [],
            color="success",
            id = "saving-successful",
            fade = False,
            is_open = False,
            dismissable = True
        ),
        dbc.Alert(
            ['Something went wrong. Try again'],
            color="danger",
            id = "saving-error",
            fade = False,
            is_open = False,
            dismissable = True
        )
    ]
)

flask_server = Flask(__name__)
app = dash.Dash(__name__, server = flask_server, external_stylesheets=[dbc.themes.BOOTSTRAP])

read_new_file(filepath)

app.layout = html.Div([
        html.Div([
            dcc.Graph(figure=go.Figure(), style={'width': '80vw', 'height': '80vh'}, id = 'main_graph'),
            graph_alerts
        ]),      
    html.Div([
        dbc.Button('Back', id = 'back', color = 'secondary',
                        className = 'me-1'),
        dbc.Button('Mark selected as CS and save!', id = 'save', color = 'primary',
                        className = 'me-1'),
        dbc.Button('Next', id = 'next', color = 'secondary',
                        className = 'me-1'),
        dbc.Button('Change data source', id = 'cdata', color = 'danger',
                        className = 'me-1'),
        ], style = {'padding-left':'100px'}),
    html.Div(id = 'status', children = []), 
    io_alerts,
    dcc.Store(id = 'day', data = 0),
    dcc.Store(id = 'reset', data = 0)],    
)

def get_df_plot(day):
    try:
        df_plot = df[df['date']==days[day]]
        df_plot.reset_index(drop=True, inplace=True)
        date = df_plot['date'].iloc[0]
        location = df_plot['LocationID'].iloc[0]
        return df_plot, date, location
    except IndexError:
        raise

def next_path():
    files = os.listdir(in_dir)
    current_idx = 0
    for idx, file in enumerate(files):
        if file == filename:
            current_idx = idx
            break
    
    new_idx = current_idx + 1
    while not files[new_idx].endswith('.csv'):
        new_idx += 1
    new_path = os.path.join(in_dir, files[new_idx])
    print(new_path)
    return new_path
    

@app.callback(
    Output('reset', 'data'),
    State('reset', 'data'),
    Input('cdata', 'n_clicks')
)
def update_data(resets, clicks):
    trigger = ctx.triggered_id
    if trigger == 'cdata':
        resets = int(resets)
        filepath = next_path()
        read_new_file(filepath)
        resets += 1
        return resets
    return dash.no_update


@app.callback(
    Output('main_graph', 'figure'),
    Output('alert-end-of-dataframe', 'is_open'),
    Input('day', 'data')
) 
def plot(day):
    try:
        df_plot, date, location = get_df_plot(int(day))
        
        # check which cs models are in the dataframe
        cs_models = []
        for model in maximum_cs_models:
            if model in df_plot.columns:
                cs_models.append(model)
        
        # plot with cs models found
        fig = plotly_plot(df_plot, measured, cs_models, 
                       date, location)
    except IndexError:
        return dash.no_update, True
    return fig, dash.no_update
    

@app.callback(
    Output('day', 'data'),
    State('day', 'data'),
    Input('next', 'n_clicks'),
    Input('back', 'n_clicks'),
    Input('reset', 'data')
)
def update_day(day, clicks_next, clicks_back, reset):
    button_clicked = ctx.triggered_id
    day = int(day)
    if button_clicked == 'back':
        day -= 1
    elif button_clicked == 'reset':
        day = 0
    else:
        day += 1
    return day   

def save_file(location, date, df_plot):
    filename = 'cs_manual_{}_{}.csv'.format(location, date)
    filepath = os.path.join(out_dir, filename)

    cfg.makedir(out_dir)
    
    df_plot.to_csv(filepath, index = False)
    
    if os.path.isfile(filepath):
        return 0, filename
    return -1, filename


@app.callback(
    Output("saving-successful", "children"),
    Output("saving-successful", "is_open"),
    Output("saving-error", "is_open"),
    State("main_graph", "selectedData"),
    State('day', 'data'),
    Input("save", "n_clicks"),
    Input('main_graph', 'figure')
)
def callback(selection, day, clicks, fig):
    trigger = ctx.triggered_id
    if trigger is None:
        return dash.no_update, dash.no_update, dash.no_update
    
    if trigger == 'main_graph':
        return dash.no_update, False, False

    if selection is not None:
        selectedpoints = list(set([p['pointIndex'] for p in selection['points']]))
    else:
        selectedpoints = []
    
    df_plot, date, location = get_df_plot(int(day))
    df_plot['manually_cs_labelled'] = np.where(df_plot.index.isin(selectedpoints), True, False)

    status, filename = save_file(location, date, df_plot)

    if status == 0:
        return 'Successfully saved as {} in {}'.format(filename, out_dir), True, False
    else:
        return dash.no_update, False, True


def open_browser():
    webbrowser.open_new("http://localhost:{}".format(8050))

if __name__ == '__main__':
    Timer(1, open_browser).start()
    serve(app.server, host = '127.0.0.1', port = 8050)
