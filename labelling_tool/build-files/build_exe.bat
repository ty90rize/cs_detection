Rem before executing, ensure that the config folder and labelling_tool.py file 
Rem are in the same directory as this batch file.
Rem Then you may execute this batch file.
Rem The .exe file will be located in the dist folder

call python -m venv %HOMEDRIVE%%HOMEPATH%\labtool
call %HOMEDRIVE%%HOMEPATH%\labtool\Scripts\Activate
call python -m pip install -r requirements.txt
call pyinstaller labelling_tool.py --onefile
cd dist
mkdir config
cd ..
robocopy config dist/config /E
pause