# How to create a new .exe file

## Prepare necessary files

1. copy the `labelling_tool.py` and `config` folder to this folder (same folder as where `build_exe.bat` is located.

## Create executable
2. Run `build_exe.bat`
3. The content of the `dist` folder is all you need to run the program on Windows.