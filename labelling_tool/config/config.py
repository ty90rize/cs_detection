import configparser
import os

import tkinter as tk
from tkinter import filedialog as fd

def _get_file():
    root = tk.Tk()
    root.withdraw()
    root.update()
    file_path = fd.askopenfilename(title = 'Select the file you would like to label first.')
    parts = os.path.split(file_path)
    return parts[0], parts[1]

def _get_out_dir():
    root = tk.Tk()
    root.withdraw()
    root.update()
    dir_path = fd.askdirectory(title = 'Select the directory where your output files shall be stored.')
    return dir_path

class ConfigException(KeyError):
    def __init__(self, directory):
        self.value = f"No correct 'config.ini' file in {directory} directory; please put a config file there according to config template and start again."

    def __str__(self):
        return repr(self.value)


'''
Opens filedialogs to select files to label and plot directory.

'''
def get_config():
    config = configparser.ConfigParser()
    try:
        config.read('config/config.ini')
        cscolumns = config['DATA']['cscolumns']
        measuredcolumns = config['DATA']['measuredcolumns']
    except KeyError:
        raise ConfigException("config")

    in_path, start_file = _get_file()
    dir_path = _get_out_dir()
    config['IO'] = {
        'outputdirectory': dir_path,
        'inputdirectory': in_path,
        'filename': start_file
    }
    config['DATA'] = {
        'cscolumns': cscolumns,
        'measuredcolumns': measuredcolumns
    }
    return config
        

def get_out_dir(config):
    return r'{}'.format(config['IO']['outputdirectory'])

def get_in_dir(config):
    return r'{}'.format(config['IO']['inputdirectory'])

def get_csvfile(config):
    return config['IO']['filename']

def get_cscolumns(config):
    return config['DATA']['cscolumns'].split(',')

def get_measuredcolumns(config):
    return config['DATA']['measuredcolumns'].split(',')
