# cs_detection



## File and folder structure

Files in the folder `labelling_tool` are relevant for the execution of a tool which enables labelling of "clear-sky" periods in daily irradiance data.

All other files and folders are relevant for training or inference of the FFT-GBC clear-sky detection method.

## Setup
For executing the train or inference script, you need a python environment with the packages listed in the `environment.yml` located in the `setup` folder.

## Train script

The train script is located in the file `train_script.ipynb`. All necessary data for examplary execution are located in the repository or downloaded during the execution of the notebook.
For training the model with other data, the data has to be prepared in the same way as the data in the example.

## Inference script

The inference code is provided in the file `FFT-GBC_inference.ipynb`. The other file necessary to run inference is `inferencer_functions.py`. It will be loaded
in the jupyter notebook and is meant as a collection of private functions. The script loads the model located in the `data` folder. The model is trained on 87 features, i.e., no feature selection was applied when running the train script.
Data for execution of the inference script is NOT located in the repository. You may prepare your own data according to the specifications in the code. 
The code should be applicable for most typical irradiance datasets. Inference can be run for both labelled and unlabelled data. 
However, quantitative evaluation is - naturally - only possible with labelled data.

## Labelling tool
Important files are the located in the `labelling_tool` folder. Relevant are either the file located in `most_recent_exe` OR the labelling_tool.py. In addition, you need the `config` folder with a `config.ini` included.

#### Prerequisites

- To successfully run the program, a `config` folder with a `config.ini` must be at the same place as the `labelling_tool.exe`.
- How the `config.ini` shall look like is described in the `config_template.ini` in the repository's config folder.
- Your data (csv file) needs a column 'date' (y-m-d), 'datetime' (y-m-d H:M:S; may be timezone-aware), and 'LocationID' (str).

#### How to label

1. Start the script via double click on the .exe file or by executing the python file.
2. Select the file you would like to label first
3. Select the folder where output files shall be stored
4. Label the parts of the measured data by applying the 'Box select' tool on the top right of the user interface.
5. Once you are done for a curve, press 'Mark selected as CS and save!'
6. To label the next day, press 'Next' if you have a dataset with multiple days or 'Change data source' to select another csv file.
7. Once you are done, close the browser window and the cmd window (or terminate python script)