import math

from frechetdist import frdist
import numpy as np
import pandas as pd
from scipy.signal import get_window
from scipy.stats import pearsonr


## Set FFT parameters
# sampling rate
f_sample = 1/60 # this is fixed since we have one measurement per minute

# Define the signal and window parameters
window_shape = 'hann' #boxcar, hann, hamming, triangle
window_size = 20 # we use 20-minute time series; more --> higher resolution, longer computing time
shift_size = 1

# Define the number of FFT points
nfft = 2 ** int(np.ceil(np.log2(window_size)))
print('nfft: {}'.format(nfft))

# Define the window function
window = get_window(window_shape, window_size)

# x_axis
frequencies = [n * f_sample / nfft for n in list(range(int(nfft / 2) + 1))]
freq_fft = np.fft.rfftfreq(nfft, 60)


def get_labels(pre_labels, diff_matrix_len, window_size):
    # pre_labels are labels that were made manually for each minutely timestep but are shifted compared to fft
    '''
    Since we need windows for the fft operations, we have more than one minute (<window_size> minutes) 
    for each row in the train dataset. We thus assign the label of the minute in the middle of the
    window (int(window_size / 2)) to each row.
    ''' 
    labels = [1 if (pre_labels[idx+int(window_size/2)] == 1) else 0 
              for idx in range(diff_matrix_len-(window_size-int(window_size/2)))]
    return labels
    
def fft(nfft, window, signal):
    '''
    Extracts fft features for given time series.
    
    Parameters
    ----------
    nfft : int
        number of frequency bins
    window : np.array
        the window, resulting from window function, e.g., Hann
    signal : np.array
        the time series to be processed with fft
        
    Returns
    -------
    np.array
        an array with the (absolute and real) fft values
    '''
    if len(window) > len(signal):
        window = window[0:len(signal)]
    
    frame = signal * window
    fft = np.fft.rfft(frame, n=nfft)
    result = np.abs(fft)
    
    return result

def get_fft_matrices(input_signal, cs_signal, shift_size, window_size, window, nfft):
    '''
    Runs through a given measured and cs time series and applies a moving 
    window for which it calls the fft function.
    
    Parameters
    ----------
    input_signal : np.array
        a one-dimensional array containing measured, subsequent irradiance values
    cs_signal : np.array
        a one-dimensional array containing modelled, subsequent clear sky 
        irradiance values
    shift_size : int
        the number in minutes by which the moving window shall be shifted
    window_size : int
        the length of the window
    nfft : int
        the number of bins resulting from the fft to be applied
        
    Returns
    -------
    np.array
        a two-dimensional matrix with rows for fft applications and columns with 
        resulting fft values. Results from a measured irradiance signal.
    np.array
        a two-dimensional matrix with rows for fft applications and columns with 
        resulting fft values. Results from a clear sky irradiance signal.
    '''
    number_of_executions = math.ceil(len(input_signal) / shift_size)
    
    measured_matrix = np.zeros((number_of_executions, nfft // 2 + 1))
    cs_matrix = np.zeros((number_of_executions, nfft // 2 + 1))

    for idx in range(number_of_executions):
        signal_measured = input_signal[idx*shift_size:idx*shift_size+window_size]
        signal_cs = cs_signal[idx*shift_size:idx*shift_size+window_size]

        fft_measured = fft(nfft, window, signal_measured)
        fft_cs = fft(nfft, window, signal_cs)

        measured_matrix[idx, :] = fft_measured
        cs_matrix[idx, :] = fft_cs    
    
    return measured_matrix, cs_matrix


def get_frechet(input_signal, cs_signal, shift_size, window_size):
    '''
    Runs through a given measured and cs time series and applies a moving 
    window for which it calculates frechet distance.
    
    Parameters
    ----------
    input_signal : np.array
        a one-dimensional array containing measured, subsequent irradiance values
    cs_signal : np.array
        a one-dimensional array containing modelled, subsequent clear sky 
        irradiance values
    shift_size : int
        the number in minutes by which the moving window shall be shifted
    window_size : int
        the length of the window
        
    Returns
    -------
    np.array
        frechet dist.
    '''
    number_of_executions = math.ceil(len(input_signal) / shift_size)
    
    frechet_matrix = np.zeros(number_of_executions)

    for idx in range(number_of_executions):
        signal_measured = input_signal[idx*shift_size:idx*shift_size+window_size]
        signal_cs = cs_signal[idx*shift_size:idx*shift_size+window_size]
        idxs = list(range(len(signal_measured)))
        
        input_measured = list(zip(idxs, signal_measured))
        input_cs = list(zip(idxs, signal_cs))
        
        frechet = frdist(input_measured, input_cs)

        frechet_matrix[idx] = frechet
    
    return frechet_matrix


def get_correlation(input_signal, cs_signal, shift_size, window_size):
    '''
    Calculates person correlation for measured and clear-sky signal for a given window.
    
    Parameters
    ----------
    input_signal : np.array
        a one-dimensional array containing measured, subsequent irradiance values
    cs_signal : np.array
        a one-dimensional array containing modelled, subsequent clear sky 
        irradiance values
    shift_size : int
        the number in minutes by which the moving window shall be shifted
    window_size : int
        the length of the window
        
    Returns
    -------
    np.array
        pearson correlation coefficient
    '''
    number_of_executions = math.ceil(len(input_signal) / shift_size)
    
    correl_matrix = np.zeros(number_of_executions)

    for idx in range(number_of_executions):
        signal_measured = input_signal[idx*shift_size:idx*shift_size+window_size]
        signal_cs = cs_signal[idx*shift_size:idx*shift_size+window_size]
        
        try:
            correl = pearsonr(signal_measured, signal_cs)[0]
        except ValueError:
            #print('Lengths: measured: {}, cs: {}'.format(
            #        len(signal_measured), len(signal_cs)))
            correl = np.nan

        correl_matrix[idx] = correl
    
    return correl_matrix

def get_base_features(measured_matrix, cs_matrix):
    # difference between fft from measured data and fft from clear sky data
    diff_matrix = measured_matrix - cs_matrix

    # remove sign before feature set one
    diff_matrix_abs = np.abs(diff_matrix)

    # Feature set: relative difference between fft from measured data and fft from clear sky data
    rel_diff_matrix = diff_matrix_abs / cs_matrix
    
    return rel_diff_matrix    

def exclude_invalid_indices(df_ml_daily, invalid_indices, labels):
    df_ml_daily = df_ml_daily[:len(labels)]
    df_ml_daily['is_cs'] = labels
    invalid_indices = list(set(invalid_indices))
    df_ml_daily = df_ml_daily[~df_ml_daily.index.isin(df_ml_daily.iloc[invalid_indices].index)]
    df_ml_daily.reset_index(drop = True, inplace = True)
    return df_ml_daily

def get_deviation_matrix(shift, rel_diff_matrix, freq_fft):
    shift_matrix = np.roll(rel_diff_matrix, shift, axis = 0)
    deviation_shift_matrix = np.abs(shift_matrix - rel_diff_matrix)
    feature_columns = ['deviation_{}_freq_{}'.format(shift, f) for f in freq_fft]
    return deviation_shift_matrix, feature_columns   

def extract_features(daily_df, ghi_column, ghi_cs_column, ground_truth_column): 
    signal_measured = np.array(daily_df[ghi_column])
    signal_cs = np.array(daily_df[ghi_cs_column])

    invalid_indices = [] # indices for minutes that cannot appropriately be predicted due to windowing
    columns = [] # column names for features

    # extract base features
    measured_matrix, cs_matrix = get_fft_matrices(signal_measured, signal_cs, shift_size, window_size, 
                                                  window, nfft)        
    rel_diff_matrix = get_base_features(measured_matrix, cs_matrix)

    # get frechet
    frechet = get_frechet(signal_measured, signal_cs, shift_size, window_size)
    columns.append('frechetdist')

    # get correlation
    correl = get_correlation(signal_measured, signal_cs, shift_size, window_size)
    columns.append('correlation')

    # The last valid label is a window length before the end of the dataset, since only then we have a full window following
    # Thus, we have to remove the window_size values from all data and feature sets
    invalid_indices += [(elem+1)*(-1) for elem in list(range(int(window_size/2)))] # those become invalid due to np.roll
    invalid_indices += [elem for elem in list(range(int(window_size/2)))]
    columns += ['rel_diff_freq_{}'.format(f) for f in freq_fft]
    
    # Feature: Deviation of window fft from window fft before
    rel_diff_matrix_before, columns_before = get_deviation_matrix(1, rel_diff_matrix, freq_fft)
    columns += columns_before
    invalid_indices += [max(invalid_indices) + 1]
    
    # Feature: Deviation of window fft from window fft before
    rel_diff_matrix_2before, columns_2before = get_deviation_matrix(2, rel_diff_matrix, freq_fft)
    columns += columns_2before
    invalid_indices += [max(invalid_indices) + 1]

    # Feature: Deviation of window fft from window fft before
    rel_diff_matrix_after, columns_after = get_deviation_matrix(-1, rel_diff_matrix, freq_fft)
    columns += columns_after 
    invalid_indices += [min(invalid_indices) - 1]
    
    # Feature: Deviation of window fft from window fft before
    rel_diff_matrix_2after, columns_2after = get_deviation_matrix(-2, rel_diff_matrix, freq_fft)
    columns += columns_2after
    invalid_indices += [min(invalid_indices) - 1]
    
    # get labels
    if ground_truth_column is not None:
        labels = get_labels(np.array(daily_df[ground_truth_column]), len(rel_diff_matrix), window_size)
    else:
        labels = [0.5 for idx in range(len(rel_diff_matrix)-(window_size-int(window_size/2)))]

    # build daily ml df
    wide_array = np.hstack((rel_diff_matrix, frechet.reshape(len(frechet), 1), correl.reshape(len(correl), 1),
                           rel_diff_matrix_before, rel_diff_matrix_2before, rel_diff_matrix_after, rel_diff_matrix_2after))
    df_ml_daily = pd.DataFrame(data = wide_array, columns = columns)

    # keep original index             
    df_ml_daily['old_idx'] = daily_df.index + int(window_size/2) # we later want to map the mid-window minute with the label
    df_ml_daily = exclude_invalid_indices(df_ml_daily, invalid_indices, labels)
    return df_ml_daily